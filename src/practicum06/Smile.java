package practicum06;

import java.util.Arrays;
import java.util.List;

import lib.TextIO;

public class Smile {
	
	private static List<String> acceptedSmiles = Arrays.asList(
		"smile",
		":)",
		":-)",
		"(:",
		";)"
	);
	
	public static boolean getSmile() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Give me a smile, please :)");
			
			String input = TextIO.getlnString().trim();
			if (acceptedSmiles.contains(input)) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		if (getSmile()) {
			System.out.println("Thank you :)");
		}
		else {
			System.out.println("What's wrong with you :/");
		}
		
	}
}
