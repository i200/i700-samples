package practicum10;

import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class WindowExample implements Runnable {

	@Override
	public void run() {		
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		window.setSize(200, 200);
		
		window.setLayout(new FlowLayout());
		
		JTextArea area = new JTextArea(5, 10);
		window.add(area);
		
		JButton button = new JButton("Hello!");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("click");
			}
		});
		window.add(button);		
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new WindowExample());
	}

}
