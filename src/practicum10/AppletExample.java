package practicum10;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JApplet;

@SuppressWarnings("serial")
public class AppletExample extends JApplet {
	
	@Override
	public void init() {
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				AppletExample.this.repaint();				
			}
		}, 500, 500);
	}

	
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.black);
		int w = getWidth();
		int h = getHeight();
		
		g.fillRect(0, 0, w, h);
		
		g.setColor(Color.yellow);
		for (int i = 0; i < 100; i++) {
			int x = (int) (w * Math.random());
			int y = (int) (h * Math.random());
			int size = (int) (5 * Math.random());
			g.fillOval(x, y, size, size);			
		}
		
	}
}

