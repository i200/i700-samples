package windows;

import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class First extends JPanel {
	
	JButton button = new JButton("Go to second view!");
	
	public First(MouseListener mouseListener) {
		
		add(new JLabel("This is first"));
		button.addMouseListener(mouseListener);
		add(button);
	}
}