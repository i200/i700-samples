package windows;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;


public class CreateWindow implements Runnable {

	JFrame win = new JFrame();
	First first = new First(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent e) {
			win.getContentPane().removeAll();
			win.setContentPane(second);
			win.validate();
			win.repaint();
		}
	});
	Second second = new Second();
	
	@Override
	public void run() {
		win.setSize(300, 300);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setContentPane(first);
		win.setVisible(true);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new CreateWindow());
	}
}
