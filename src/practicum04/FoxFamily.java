package practicum04;

public class FoxFamily {
	public static void main(String[] args) {
		
		for (int i = 1; i < 10; i = i + 1) {
			System.out.println("Fox #" + i + ": ");
			for (int j = 1; j <= 3; j = j + 1) {
				System.out.println("\tFox #" + i + " child #" + j);
			}
		}
	}

}