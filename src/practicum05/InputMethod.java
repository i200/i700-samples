package practicum05;

import lib.TextIO;

public class InputMethod {
	
	public static String getNotEmptyInput(String question) {
		while (true) {
			System.out.println(question);
			String input = TextIO.getlnString();
			if (!input.trim().equals("")) {
				return input;
			}
			System.out.println("Empty input is not allowed");
		}
	}
	
	public static void main(String[] args) {
		String name = getNotEmptyInput("Insert a name:");
		System.out.format("Wow, what a coincidence! My name is %s as well.", name);
	}

}
