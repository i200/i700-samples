package test;

import lib.TextIO;

public class SixChars {

	public static void main(String[] args) {
		
		int charCount = 6;
		{
			System.out.println("Variant 1");
			System.out.format("Insert %d characters: ", charCount);
			String input = TextIO.getlnString();
			if (input.length() > charCount) {
				input = input.substring(0, charCount);
			}
			
			System.out.format("Inserted: %s\n\n", input);
		}
		
		{
			System.out.println("Variant 2");
			String input;
			while (true) {
				System.out.format("Insert %d characters: ", charCount);
				input = TextIO.getlnString();
				int length = input.length();
				if (length == charCount) {
					break;
				}
				else if (length > charCount) {
					System.out.println("Too long input");
				}
				else {
					System.out.println("Too short input");
				}
			}
			System.out.format("Inserted: %s", input);
		}
	}
	
}
