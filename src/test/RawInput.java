package test;

import java.io.Console;
import java.io.IOException;
import java.io.Reader;

public class RawInput {
	
	public static void executeCommand(String[] command) {
		try {
			Runtime.getRuntime().exec(command).waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void rawTerminal() {
		executeCommand(new String[]{"/bin/sh", "-c", "stty raw </dev/tty"});
	}
	public static void saneTerminal() {
		executeCommand(new String[] {"/bin/sh", "-c", "stty sane </dev/tty"});
	}
	
	public static void main(String[] args) {
		rawTerminal();
		Console console = System.console();
		if (console == null) {
			System.out.println("Execute this file from an Unix command line:");
			System.out.println("Go to Eclipse's project's bin directory and type:\n");
			System.out.println("java test.RawInput");
			return;
		}
		Reader reader = console.reader();
		
		String result = "";
		int charCount = 6;
		System.out.format("Insert %d characters\r\n", charCount);
		for (int i = 0; i < charCount; i++) {
			try {
				result += (char)reader.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\r");
		System.out.println("Inserted: " + result + "\r");
		saneTerminal();
	}

}
